
Get all permissions declared in all APKs in all known fdroid repos

```console
$ ./get-all-permissions-from-fdroid-repos.py > all-permissions-from-fdroid-repos.csv
```
